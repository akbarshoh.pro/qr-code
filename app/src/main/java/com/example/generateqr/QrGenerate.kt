package com.example.generateqr

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View

class QrGenerate @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
): View(context, attrs, defStyle, defStyleRes) {
    var matrix = ArrayList<ArrayList<Int>>()
        set(value) {
            field = value
            Log.d("TTT", "${matrix.size}")
            invalidate()
        }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        generateRectangle(canvas)
    }

    private fun generateRectangle(canvas: Canvas) {
        if (matrix.isNotEmpty()) {
            val width = width.toFloat() / matrix[0].size
            val height = height.toFloat() / matrix.size

            repeat(matrix.size) {i ->
                repeat(matrix[0].size) {j ->
                    val left = j * width
                    val top = i * height
                    if (matrix[i][j] == 1)
                        canvas.drawRect(left, top , left + width, top + height, rectOnePaint)
                    else if (matrix[i][j] == 0)
                        canvas.drawRect(left, top , left + width, top + height, rectZeroPaint)
                    else
                        canvas.drawRect(left, top , left + width, top + height, rectTwoPaint)
                }
            }
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val width = width.toFloat() / matrix[0].size
        val height = height.toFloat() / matrix.size
        val x = (event.x / width).toInt()
        val y = (event.y / height).toInt()
        Log.d("TTT", "${x} / ${y}")
        if (matrix[y][x] == 0) {
            recurs(x, y)
            invalidate()
        }
        return super.onTouchEvent(event)
    }

    private fun recurs(x: Int, y: Int) {
        if (x < 0 || x >= matrix[0].size || y < 0 || y >= matrix.size || matrix[y][x] == 1 || matrix[y][x] == 2) return
        else {
            matrix[y][x] = 2
            invalidate()
            postDelayed({
                recurs(x + 1, y)
                recurs(x, y + 1)
                recurs(x - 1, y)
                recurs(x, y - 1)
            }, 500)
        }
    }

    private val rectOnePaint = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.FILL
        color = Color.BLACK
    }

    private val rectZeroPaint = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.FILL
        color = Color.WHITE
    }

    private val rectTwoPaint = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.FILL
        color = Color.BLUE
    }
}