package com.example.generateqr.presentaion.screen.qr

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.example.generateqr.R
import com.example.generateqr.databinding.ScreenQrBinding
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.math.ceil
import kotlin.random.Random

class QrScreen : Fragment(R.layout.screen_qr) {
    private lateinit var binding: ScreenQrBinding
    private var width = 0
    private var height = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ScreenQrBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.generate.setOnClickListener {
            if (binding.verCountInput.text.toString() != "" && binding.horCountInput.text.toString() != "") {
                width = binding.verCountInput.text.toString().toInt()
                height = binding.horCountInput.text.toString().toInt()
            }
            generate()
        }

    }

    private fun generate() {
        val matrix: ArrayList<ArrayList<Int>> = ArrayList()
        repeat(height) {i ->
            matrix.add(ArrayList())
            repeat(width) {j ->
                val num = Random.nextInt(2)
                matrix[i].add(num)
            }
        }
        Log.d("TTT", "${matrix.size} / ${matrix[0].size}")
        binding.containerQr.matrix = matrix
    }

}